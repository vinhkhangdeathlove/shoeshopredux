import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
} from "./redux/constant/shoeShopConstant";

class TableGioHang extends Component {
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.cart.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleChangeQuantity(item.id, -1);
                      }}
                      className="btn btn-primary"
                    >
                      -
                    </button>
                    <span>{item.soLuong}</span>
                    <button
                      onClick={() => {
                        this.props.handleChangeQuantity(item.id, 1);
                      }}
                      className="btn btn-primary"
                    >
                      +
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleRemoveShoe(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (id, step) => {
      return dispatch({
        type: CHANGE_QUANTITY,
        payload: {
          id: id,
          step: step,
        },
      });
    },
    handleRemoveShoe: (id) => {
      return dispatch({
        type: REMOVE_FROM_CART,
        payload: id,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TableGioHang);
