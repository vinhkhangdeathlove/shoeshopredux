import { shoeArr } from "../../data_shoeShop";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
} from "../constant/shoeShopConstant";

let initialState = {
  shoeArr: shoeArr,
  cart: [],
};

export let shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.cart.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneCart = [...state.cart];
      if (index === -1) {
        let newShoe = { ...payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case CHANGE_QUANTITY: {
      let index = state.cart.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneCart = [...state.cart];
      cloneCart[index].soLuong += payload.step;
      if (cloneCart[index].soLuong === 0) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case REMOVE_FROM_CART: {
      let index = state.cart.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneCart = [...state.cart];
      cloneCart.splice(index, 1);
      state.cart = cloneCart;
      return { ...state };
    }
    default: {
      return state;
    }
  }
};
