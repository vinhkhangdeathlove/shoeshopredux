import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShopReducer";

export let rootReducer = combineReducers({ shoeShopReducer });
