import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constant/shoeShopConstant";

class ListShoe extends Component {
  render() {
    return (
      <div className="row">
        {this.props.shoeArr.map((item) => {
          return (
            <div className="col-4 my-5">
              <div className="card" style={{ width: "18rem" }}>
                <img src={item.image} className="card-img-top" alt="" />
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <p className="card-text">{item.description}</p>
                  <button
                    onClick={() => {
                      this.props.handleAddShoe(item);
                    }}
                    className="btn btn-primary"
                  >
                    Add to cart
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeShopReducer.shoeArr,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddShoe: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListShoe);
