import React, { Component } from "react";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";

export default class ShoeShop extends Component {
  render() {
    return (
      <div className="container py-5">
        <TableGioHang />
        <ListShoe />
      </div>
    );
  }
}
